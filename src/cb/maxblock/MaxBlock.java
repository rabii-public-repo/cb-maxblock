package cb.maxblock;

import java.util.TreeSet;

public class MaxBlock {
	/*
	 * @see https://codingbat.com/prob/p179479
	 */
	public static void main(String[] args) {
		System.out.println(maxBlock("abbCCCddBBBxx"));

	}

	static int maxBlock(String str) {
		int count, i = 0;
		TreeSet<Integer> treeSet = new TreeSet<>();
		while (i < str.length()) {
			count = 1;
			while (hasAdjacent(i, str)) {
				i++;
				count++;
			}
			treeSet.add(count);
			i++;
		}
		if (treeSet.isEmpty())
			return 0;
		return treeSet.last();
	}

	static boolean hasAdjacent(int index, String str) {
		if (index + 1 < str.length())
			return str.charAt(index + 1) == str.charAt(index);
		return false;
	}
}
